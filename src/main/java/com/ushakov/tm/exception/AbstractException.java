package com.ushakov.tm.exception;

public class AbstractException extends RuntimeException {

    protected String message;

    protected AbstractException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
