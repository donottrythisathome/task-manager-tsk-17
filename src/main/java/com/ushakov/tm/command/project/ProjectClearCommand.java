package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Clear project list.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        serviceLocator.getProjectService().clear();
    }

}
