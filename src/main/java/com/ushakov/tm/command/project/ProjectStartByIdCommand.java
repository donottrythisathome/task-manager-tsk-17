package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;

public class ProjectStartByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "start-project-by-id";
    }

    @Override
    public String description() {
        return "Start project by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().startProjectById(projectId);
        if (project == null) throw new ProjectNotFoundException();
    }

}
