package com.ushakov.tm.command.system;

import com.ushakov.tm.command.AbstractCommand;

import java.util.List;

public class HelpCommand extends AbstractCommand {
    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "Show terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final List<String> names = serviceLocator.getCommandService().getCommandNames();
        for (final String name: names) {
            System.out.println(name);
        }
        System.out.println();
    }

}
