package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

public class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "complete-task-by-index";
    }

    @Override
    public String description() {
        return "Complete task by index.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        final Task task = serviceLocator.getTaskService().completeTaskByIndex(taskIndex - 1);
        if (task == null) throw new TaskNotFoundException();
    }

}
