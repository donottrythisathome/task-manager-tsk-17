package com.ushakov.tm.api.service;

import com.ushakov.tm.command.AbstractCommand;
import com.ushakov.tm.model.Command;

import java.util.List;

public interface ICommandService {

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    List<AbstractCommand> getCommands();

    List<String> getCommandNames();

    void add(AbstractCommand command);

}
