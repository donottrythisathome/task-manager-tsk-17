package com.ushakov.tm.api.repository;

import com.ushakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project removeOneByName(String name);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Project findOneByName(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

}
