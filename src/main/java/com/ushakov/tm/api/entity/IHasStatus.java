package com.ushakov.tm.api.entity;

import com.ushakov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
