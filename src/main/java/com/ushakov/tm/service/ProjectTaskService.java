package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.api.repository.ITaskRepository;
import com.ushakov.tm.api.service.IProjectTaskService;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTasksByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProjectId(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

    @Override
    public Project deleteProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        final List<Task> taskList = taskRepository.findAllByProjectId(projectId);
        for (final Task task: taskList) {
            taskRepository.removeOneById(task.getId());
        }
        return projectRepository.removeOneById(projectId);
    }

}
